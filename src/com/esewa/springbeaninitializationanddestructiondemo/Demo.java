package com.esewa.springbeaninitializationanddestructiondemo;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("resource/applicationContext.xml");


       Message obj = (Message) context.getBean("messagebean");
        obj.getMessage();
        context.registerShutdownHook();
    }
}
